package db

import (
	"context"
	log "github.com/sirupsen/logrus"
	"broker-template/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func GetMongoClient(cfg config.Config) *mongo.Client {
	log.Info("[GetMongoClient] Initialize mongo client")
	client, err := mongo.NewClient(options.Client().ApplyURI(cfg.MongoConfig().GetUrl()))

	if err != nil {
		log.Fatalf("Error while try to initialize mongo db %v", err)
	}

	// Create connect
	err = client.Connect(context.TODO())
	if err != nil {
		log.Fatalf("Error while connect to mongodb %v", err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatalf("Error while ping to mongodb %v", err)
	}
	return client
}
