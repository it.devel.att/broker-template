package db

import (
	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
	"broker-template/config"
)

func GetRedisClient(cfg config.Config) *redis.Client {
	log.Info("[GetRedisClient] Initialize redis client")
	opt, err := redis.ParseURL(cfg.RedisConfig().GetUrl())

	if err != nil {
		log.Fatal("Error while try to parse redis url")
	}

	return redis.NewClient(opt)
}
