FROM golang:1.13.5-alpine as build
WORKDIR /src
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o broker-template .

FROM scratch as bin
COPY --from=build /src/broker-template ./broker-template
CMD ["./broker-template"]
