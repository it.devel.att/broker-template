package settings

import (
	"context"
	"broker-template/config"
	"broker-template/repository"
	"sync"
)

type Shared interface {
	GetWaitGroup() *sync.WaitGroup
	GetContext() context.Context
	GetConfig() config.Config
	GetRepository() repository.Repository
}

type Settings struct {
	WaitGroup  *sync.WaitGroup
	Context    context.Context
	Config     config.Config
	Repository repository.Repository
}

func GetNewShared(ctx context.Context, cfg config.Config, rep repository.Repository) Shared {
	return &Settings{
		WaitGroup:  &sync.WaitGroup{},
		Context:    ctx,
		Config:     cfg,
		Repository: rep,
	}
}

func (s Settings) GetWaitGroup() *sync.WaitGroup {
	return s.WaitGroup
}

func (s Settings) GetContext() context.Context {
	return s.Context
}

func (s Settings) GetConfig() config.Config {
	return s.Config
}

func (s Settings) GetRepository() repository.Repository {
	return s.Repository
}
