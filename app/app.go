package app

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"broker-template/config"
	"broker-template/db"
	"broker-template/repository"
	"broker-template/settings"
	"broker-template/workerpool"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type App struct {
	shared      settings.Shared
	cancelContext context.CancelFunc
}

func initLogger(cnf config.Config) {
	logLevel, err := log.ParseLevel(cnf.GetLogLevel())
	if err != nil {
		log.SetLevel(log.ErrorLevel)
		log.Errorf("Error while trying to parse level from config: %v", err)
	} else {
		log.SetLevel(logLevel)
	}
}

func NewApp() App {
	cfg := config.NewConfig()
	initLogger(cfg)

	redisClient := db.GetRedisClient(cfg)
	mongoClient := db.GetMongoClient(cfg)

	ctx, cancel := context.WithCancel(context.Background())

	rep := repository.NewRepository(cfg, ctx, mongoClient, redisClient)

	return App{
		shared:      settings.GetNewShared(ctx, cfg, rep),
		cancelContext: cancel,
	}
}

func (app App) runWorkerPools() {
	log.Info("[App.runWorkerPools] Start worker pools")
	for _, pool := range workerpool.GetWorkerPools(app.shared) {
		pool.Run()
	}
}
func (app App) Run() {
	log.Info("[App.Run] Run application")
	app.runWorkerPools()
}

func (app App) WaitForStop() {
	termChan := make(chan os.Signal)
	signal.Notify(termChan, syscall.SIGINT, syscall.SIGTERM)
	log.Info("[App.WaitForStop] Wait application to stop")
	<-termChan
	fmt.Println()
	log.Info("[App.WaitForStop] Receive interrupt signal")

	start := time.Now()
	app.cancelContext()
	app.shared.GetWaitGroup().Wait()
	elapsed := time.Since(start)

	log.Info("[App.WaitForStop] Time for shutdown: %v", elapsed)
}
