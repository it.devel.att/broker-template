module broker-template

go 1.13

require (
	github.com/caarlos0/env/v6 v6.4.0
	github.com/go-redis/redis/v8 v8.4.4
	github.com/sirupsen/logrus v1.4.2
	go.mongodb.org/mongo-driver v1.4.4
)
