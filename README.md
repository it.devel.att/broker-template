# Пример брокера сообщений с пулом воркеров и корректным завершением работы (graceful shutdown)

Структура проекта
- app
  
  Здесь находится вся инициализация нашего приложения (точка входа)
- config
  
  Здесь находятся конфиги приложения с возможностью проброса их через переменные окружения
  > Такие переменные как строка подключения к базе данных, кол-во воркеров, тайм-ауты на запросы в бд, токены сервисов и тд.
- db
  
  Здесь происходит инициализия клиентов к базам данных
- repository
  
  Здесь хранится логика для взаимодействия с хранилищами данных (redis, mongo и тд.)
  > Запросы в бд и тд (возможно межсетевые запросы тоже можно добавить сюда)
- settings
  
  Здесь хранится структура с данным которые используются на многих уровнях проекта (shared preferences)
- worker
  
  Здесь создаются Воркеры с логикой своей работы
- workerpool
  
  Здесь для каждого воркера создаётся воркерпул для запуска воркеров


### Для демонстрации работы приложения создан воркер multiplier который возводит в квадрат полученное число и сохраняет результат в монго бд

Для примера запустим наше приложение

```
make start
```
* Это поднимет докер компоуз с монгой и редисом с проброшенными портами на хост
* Сбилдит бинарный файл приложения и запустит его
```
docker-compose up -d
Creating broker-template_redis_1 ... done
Creating broker-template_mongo_1 ... done
go build -o broker-template -v
./broker-template
2021/01/08 14:37:04 [NewConfig] Initialize configs
2021/01/08 14:37:04 [GetRedisClient] Initialize redis client
2021/01/08 14:37:04 [GetMongoClient] Initialize mongo client
2021/01/08 14:37:06 [NewRepository] Initialize repositories
2021/01/08 14:37:06 [App.Run] Run application
2021/01/08 14:37:06 [App.runWorkerPools] Start worker pools
2021/01/08 14:37:06 [MultiplierWorkerPool.Run] Run MultiplierWorkerPool. MultiplierWorker quantity: 5
2021/01/08 14:37:06 [App.WaitForStop] Wait application to stop
```
> Примерный вывод консоли при запуске команды ***make start***

Теперь наши воркеры запущены и ждут данные в обработку

Для примера создадим какой-нибудь продьюсер данных который будет слать данные в редис
```ruby
require 'redis'
redis = Redis.new(url: "redis://localhost:6379/0")

1000.times do |i|
    key = "NUMBERS"
    if i % 3 == 0
      redis.lpush(key, "some wrong data #{i}") # For example of dirty data
    else
      redis.lpush(key, i)
    end
    puts "Send task #{i} to key #{key}"
end
```
> Пример написан на руби. Просто шлёт цифры в редис ключ ***NUMBERS*** типа Lists
Скопируйте скрипт в какой-нибудь файл и запустите его. (Или можете написать свой приме продьюсера)

```
ruby producer.rb
```
Как только данные появились в редисе, воркеры начнут их обрабатывать
```
2021/01/08 14:48:05 [MultiplierWorkerPool.getData] error while strconv.Atoi result: strconv.Atoi: parsing "some wrong data 0": invalid syntax
2021/01/08 14:48:05 [Multiplier.handle] Worker 2 start to handle number 1
2021/01/08 14:48:05 [Multiplier.handle] Worker 4 start to handle number 2
2021/01/08 14:48:05 [MultiplierWorkerPool.getData] error while strconv.Atoi result: strconv.Atoi: parsing "some wrong data 3": invalid syntax
2021/01/08 14:48:05 [Multiplier.handle] Worker 3 start to handle number 4
2021/01/08 14:48:05 [Multiplier.handle] Worker 1 start to handle number 5
2021/01/08 14:48:05 [MultiplierWorkerPool.getData] error while strconv.Atoi result: strconv.Atoi: parsing "some wrong data 6": invalid syntax
2021/01/08 14:48:05 [Multiplier.handle] Worker 5 start to handle number 7
2021/01/08 14:48:05 [MultiplierWorkerPool.getData] error while strconv.Atoi result: strconv.Atoi: parsing "some wrong data 9": invalid syntax
2021/01/08 14:48:08 [Multiplier.handle] Worker 2 finish to handle number 1
2021/01/08 14:48:08 [Multiplier.handle] Worker 2 start to handle number 8
2021/01/08 14:48:08 [Multiplier.handle] Worker 4 finish to handle number 2
2021/01/08 14:48:08 [Multiplier.handle] Worker 3 finish to handle number 4
2021/01/08 14:48:08 [Multiplier.handle] Worker 1 finish to handle number 5
2021/01/08 14:48:08 [Multiplier.handle] Worker 5 finish to handle number 7

```

### Прерывание программы
При завершении программы через CTRL+C или остановке докер контейнера с приложением будет:
* Вызов cancel функции от главного контекста приложения (***context.WithCancel(context.Background())***)
* Ожидание через WaitGroup когда воркеры завершат свою работу (***settings.WaitGroup.Wait()***)

```
2021/01/08 14:51:40 [App.WaitForStop] Wait application to stop
^C
2021/01/08 14:51:42 [App.WaitForStop] Receive interrupt signal
2021/01/08 14:51:42 [MultiplierWorkerPool.getData] error while get data: context canceled
2021/01/08 14:51:42 [MultiplierWorkerPool.Run] Receive ctx.Done()
2021/01/08 14:51:42 [App.WaitForStop] Time for shutdown: 243.465µs
```
> Примерный вывод при завершении
