package workerpool

import (
	"broker-template/settings"
)

type WorkerPool interface {
	Run()
}

// Return slice of worker pools
func GetWorkerPools(shared settings.Shared) []WorkerPool {
	return []WorkerPool{
		// Add new pools here. Do not add duplicates worker pools
		NewMultiplierWorkerPool(shared),
	}
}
