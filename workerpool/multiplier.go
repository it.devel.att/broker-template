package workerpool

import (
	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
	"broker-template/settings"
	"broker-template/worker"
	"strconv"
)

type MultiplierWorkerPool struct {
	shared settings.Shared
	numbers  chan int
	poolSize int
}

func NewMultiplierWorkerPool(shared settings.Shared) WorkerPool {
	poolAndBufferSize := shared.GetConfig().GetMultiplierWorkerPoolSize()

	return &MultiplierWorkerPool{
		shared: shared,
		poolSize: poolAndBufferSize,
		numbers:  make(chan int, poolAndBufferSize),
	}
}

func (pool MultiplierWorkerPool) Run() {
	log.Infof("[MultiplierWorkerPool.Run] Run MultiplierWorkerPool. MultiplierWorker quantity: %v ", pool.poolSize)
	pool.getData()
	pool.runWorkers()
}

func (pool MultiplierWorkerPool) getData() {
	pool.shared.GetWaitGroup().Add(1)

	go func() {
		defer pool.shared.GetWaitGroup().Done()

		for {
			select {
			case <-pool.shared.GetContext().Done():
				log.Infof("[MultiplierWorkerPool.Run] Receive ctx.Done()")
				close(pool.numbers)
				return
			default:
				result, err := pool.shared.GetRepository().GetMultiplier().GetNumber()
				if err != nil {
					if err != redis.Nil {
						log.Infof("[MultiplierWorkerPool.getData] error while get data: %v", err)
					}
					continue
				}
				number, err := strconv.Atoi(result[1])
				if err != nil {
					log.Infof("[MultiplierWorkerPool.getData] error while strconv.Atoi result: %v", err)
					continue
				}
				pool.numbers <- number
			}
		}
	}()
}

func (pool MultiplierWorkerPool) runWorkers() {
	for workerNumber := 1; workerNumber <= pool.poolSize; workerNumber++ {
		worker.NewMultiplierWorker(
			pool.shared, workerNumber, pool.numbers,
		).Run()
	}
}
