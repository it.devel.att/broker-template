package config

import (
	"github.com/caarlos0/env/v6"
	"log"
)

type (
	RedisConfig interface {
		GetUrl() string
		GetKey() Key
	}
	Key interface {
		GetNumbers() string
	}

	redisConfig struct {
		Url string `env:"REDIS_URL" envDefault:"redis://127.0.0.1:6379/0"`
		Key Key
	}
	key struct {
		Numbers string `env:"REDIS_KEY_NUMBERS" envDefault:"NUMBERS"`
	}
)

func GetRedisConfig() RedisConfig {
	cfg := &redisConfig{}
	if err := env.Parse(cfg); err != nil {
		log.Fatalf("[GetRedisConfig] Error while try to parse env for RedisConfig: %v", err)
	}
	cfg.Key = GetRedisKeyConfig()
	return cfg
}

func GetRedisKeyConfig() Key {
	cfg := &key{}
	if err := env.Parse(cfg); err != nil {
		log.Fatalf("[GetRedisKeyConfig] Error while try to parse env for RedisConfig.Key: %v", err)
	}
	return cfg
}

func (r redisConfig) GetUrl() string {
	return r.Url
}

func (r redisConfig) GetKey() Key {
	return r.Key
}

func (k key) GetNumbers() string {
	return k.Numbers
}
