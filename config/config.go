package config

import (
	"github.com/caarlos0/env/v6"
	"log"
)

type Config interface {
	RedisConfig() RedisConfig
	MongoConfig() MongoConfig
	GetMultiplierWorkerPoolSize() int
	GetLogLevel() string
}

type config struct {
	Redis                    RedisConfig
	Mongo                    MongoConfig
	MultiplierWorkerPoolSize int    `env:"MULTIPLIER_WORKER_POOL_SIZE" envDefault:"5"`
	LogLevel                 string `env:"LOG_LEVEL" envDefault:"info"`
}

func NewConfig() Config {
	log.Println("[NewConfig] Initialize configs")
	cfg := &config{}
	if err := env.Parse(cfg); err != nil {
		log.Fatalf("[NewConfig] Error while try to parse env for Config: %v", err)
	}
	cfg.Mongo = GetMongoConfig()
	cfg.Redis = GetRedisConfig()
	return cfg
}

func (c config) RedisConfig() RedisConfig {
	return c.Redis
}

func (c config) MongoConfig() MongoConfig {
	return c.Mongo
}

func (c config) GetMultiplierWorkerPoolSize() int {
	return c.MultiplierWorkerPoolSize
}

func (c config) GetLogLevel() string {
	return c.LogLevel
}
