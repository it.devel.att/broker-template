package config

import (
	"github.com/caarlos0/env/v6"
	"log"
	"time"
)

type (
	MongoConfig interface {
		GetUrl() string
		GetDBName() string
		GetQueryTimeOutSeconds() time.Duration
		GetMultiplierCollectionName() string
	}

	mongoConfig struct {
		Url                      string        `env:"MONGO_URL" envDefault:"mongodb://root:root@127.0.0.1:27017"`
		DBName                   string        `env:"MONGO_DBNAME" envDefault:"main_app"`
		QueryTimeOutSeconds      time.Duration `env:"MONGO_QUERY_TIMEOUT_SECONDS" envDefault:"5s"`
		MultiplierCollectionName string        `env:"MONGO_MULTIPLIER_COLLECTION_NAME" envDefault:"multiply_results"`
	}
)

func GetMongoConfig() MongoConfig {
	cfg := &mongoConfig{}
	if err := env.Parse(cfg); err != nil {
		log.Fatalf("[GetMongoConfig] Error while try to parse env for MongoConfig: %v", err)
	}
	return cfg
}


func (m mongoConfig) GetUrl() string {
	return m.Url
}

func (m mongoConfig) GetDBName() string {
	return m.DBName
}

func (m mongoConfig) GetQueryTimeOutSeconds() time.Duration {
	return m.QueryTimeOutSeconds
}

func (m mongoConfig) GetMultiplierCollectionName() string {
	return m.MultiplierCollectionName
}
