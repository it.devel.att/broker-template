# User your own project name
PROJECT_NAME=broker-template

GOBUILD=go build
GODEPS=go mod tidy -v
GOCLEAN=go clean
GOTEST= go test


build:
	$(GOBUILD) -o ${PROJECT_NAME} -v

test:
	$(GOTEST) -v ./...

start:
	docker-compose up -d
	$(GOBUILD) -o ${PROJECT_NAME} -v
	./${PROJECT_NAME}

clean:
	$(GOCLEAN)
	rm -f ${PROJECT_NAME}

deps:
	$(GODEPS)

docker-build:
ifdef TAG
	docker build . -t ${PROJECT_NAME}:${TAG}
else
	@echo "You need to specify TAG env variable before build docker image"
	@echo "For example: TAG=test make docker-build"
endif

