package main

import "broker-template/app"

func main() {
	a := app.NewApp()
	a.Run()
	a.WaitForStop()
}
