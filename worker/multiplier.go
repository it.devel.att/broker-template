package worker

import (
	log "github.com/sirupsen/logrus"
	"broker-template/settings"
	"time"
)

type (
	Multiplier struct {
		id       int
		shared settings.Shared
		numbers  <-chan int
	}
)

func NewMultiplierWorker(
	shared settings.Shared,
	workerNumber int,
	numbers <-chan int,
) Worker {
	return &Multiplier{
		id:       workerNumber,
		shared: shared,
		numbers:  numbers,
	}
}

func (worker Multiplier) Run() {
	worker.shared.GetWaitGroup().Add(1)

	go func() {
		defer worker.shared.GetWaitGroup().Done()

		for number := range worker.numbers {
			worker.handle(number)
		}
	}()
}

// This function can contain a lot of logic with large time execution
func (worker Multiplier) handle(someNumber int) {
	log.Infof("[Multiplier.handle] Worker %v start to handle number %v", worker.id, someNumber)

	err := worker.shared.GetRepository().GetMultiplier().SaveResult(worker.id, someNumber, worker.multiply(someNumber))
	if err != nil {
		log.Infof("[Multiplier.handle] Error while save result: %v", err)
		// TODO Add some recover logic for data
	}
	time.Sleep(time.Second * 3) //Just for example
	log.Infof("[Multiplier.handle] Worker %v finish to handle number %v", worker.id, someNumber)
}

func (worker Multiplier) multiply(number int) int {
	return number * number
}
