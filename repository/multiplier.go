package repository

import (
	"context"
	"github.com/go-redis/redis/v8"
	"broker-template/config"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type (
	MultiplierRepository interface {
		GetNumber() ([]string, error)
		SaveResult(workerID int, numberGet int, numberResult int) error
	}

	multiplierRepository struct {
		collection       *mongo.Collection
		cfg              config.Config
		ctx              context.Context
		client           *redis.Client
		numberChannelKey string
	}

	multiplierResultDocument struct {
		WorkerID     int       `bson:"workerID"`
		NumberGet    int       `bson:"numberGet"`
		NumberResult int       `bson:"numberResult"`
		CreatedAt    time.Time `bson:"createdAt"`
	}
)

func NewMultiplierRepository(
	ctx context.Context,
	cfg config.Config,
	mc *mongo.Client,
	rc *redis.Client,
) MultiplierRepository {
	collection := mc.Database(cfg.MongoConfig().GetDBName()).Collection(cfg.MongoConfig().GetMultiplierCollectionName())

	return &multiplierRepository{
		collection: collection,
		cfg:        cfg,
		ctx:        ctx,
		client:     rc,
	}
}

func (mr multiplierRepository) GetNumber() ([]string, error) {
	return mr.client.BLPop(mr.ctx, time.Second, mr.cfg.RedisConfig().GetKey().GetNumbers()).Result()
}

func (mr multiplierRepository) SaveResult(workerID int, numberGet int, numberResult int) error {
	ctx, cancel := context.WithTimeout(context.Background(), mr.cfg.MongoConfig().GetQueryTimeOutSeconds())
	defer cancel()

	_, err := mr.collection.InsertOne(ctx, multiplierResultDocument{
		WorkerID:     workerID,
		NumberGet:    numberGet,
		NumberResult: numberResult,
		CreatedAt:    time.Now().UTC(),
	})
	return err
}
