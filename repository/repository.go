package repository

import (
	"context"
	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
	"broker-template/config"
	"go.mongodb.org/mongo-driver/mongo"
)

type Repository interface {
	GetMultiplier() MultiplierRepository
}

// Struct for initialize all available repositories
type repository struct {
	Multiplier MultiplierRepository
}

// Return all available repositories in Repository struct
func NewRepository(
	cfg config.Config,
	ctx context.Context,
	mongoClient *mongo.Client,
	redisClient *redis.Client,
) Repository {
	log.Info("[NewRepository] Initialize repositories")
	return &repository{
		// Add new repositories here
		Multiplier: NewMultiplierRepository(ctx, cfg, mongoClient, redisClient),
	}
}

func (r repository) GetMultiplier() MultiplierRepository {
	return r.Multiplier
}
